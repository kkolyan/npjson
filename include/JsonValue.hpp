//
// Created by kkolyan on 2021-01-20.
//

#ifndef NPJSON_JSONVALUE_HPP
#define NPJSON_JSONVALUE_HPP

#include <string>
#include <vector>
#include <ostream>
#include "JsonTypeInfo.hpp"

namespace npjson {

    class Node;

    class JsonValue {
    public:
        explicit JsonValue(Node* node);

        JsonTypeInfo getTypeInfo() const;

        JsonValue get(const std::string& property);

        JsonValue get(int index);

        int getArrayLength() const;

        std::vector<std::string> getObjectKeys() const;

        int asInt() const;

        long asLong() const;

        float asFloat() const;

        double asDouble() const;

        bool asBool() const;

        const std::string& asText() const;

        ~JsonValue();

        friend std::ostream& operator<<(std::ostream& os, const JsonValue& value);

    private:
        Node* const node;
    };

    std::ostream& operator<<(std::ostream& os, const JsonValue& value);
}

#endif //NPJSON_JSONVALUE_HPP
