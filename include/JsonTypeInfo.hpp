
//
// Created by kkolyan on 2021-01-20.
//

#ifndef NPJSON_JSONTYPEINFO_HPP
#define NPJSON_JSONTYPEINFO_HPP

#include "JsonType.hpp"

namespace npjson {
    class JsonTypeInfo {
    public:
        const JsonType type;
        const std::string& displayName;

        JsonTypeInfo(JsonType type, const std::string& displayName);

        bool operator==(const JsonTypeInfo& rhs) const;

        bool operator!=(const JsonTypeInfo& rhs) const;
    };
}

#endif //NPJSON_JSONTYPEINFO_HPP
