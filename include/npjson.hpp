//
// Created by kkolyan on 2021-01-20.
//

#ifndef NPJSON_NPJSON_HPP
#define NPJSON_NPJSON_HPP

#include "JsonValue.hpp"

namespace npjson {
    JsonValue parseFile(const std::string& fileName);
}

#endif //NPJSON_NPJSON_HPP
