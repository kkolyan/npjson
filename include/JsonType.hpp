//
// Created by kkolyan on 2021-01-25.
//

#ifndef NPJSON_JSONTYPE_H
#define NPJSON_JSONTYPE_H


enum JsonType {
    ArrayType,
    BoolType,
    NullType,
    NumberDoubleType,
    NumberLongType,
    ObjectType,
    TextType,
};


#endif //NPJSON_JSONTYPE_H
