#ifndef HELLOCPP_JOINITERABLE_H
#define HELLOCPP_JOINITERABLE_H

#include <ostream>

namespace npjson {
    template<typename T, typename C>
    inline void writeWithDelimiter(std::ostream& os, const std::string& delimiter, const T& items, C writeItem) {
        std::string d;
        for (const auto& item : items) {
            os << d;
            writeItem(os, item);
            d = delimiter;
        }
    }
}

#endif //HELLOCPP_JOINITERABLE_H