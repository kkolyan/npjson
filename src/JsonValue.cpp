//
// Created by kkolyan on 2021-01-20.
//

#include <sstream>
#include "JsonValue.hpp"
#include "ast/Node.hpp"
#include "ast/Number.hpp"
#include "ast/Text.hpp"
#include "ast/Object.hpp"
#include "ast/Array.hpp"
#include "ast/Bool.hpp"
#include "ast/NumberLong.hpp"
#include "ast/NumberDouble.hpp"
#include "ast/Null.hpp"

using namespace npjson;

static void expectType(const Node* node, const JsonTypeInfo& type) {
    if (&type != &node->getType()) {
        std::ostringstream buf;
        buf << "expected type" << type.displayName;
        node->writeToAsJson(buf);
        throw std::invalid_argument(buf.str());
    }
}

JsonValue JsonValue::get(const std::string& property) {
    if (node->getType().type != ObjectType) {
        return JsonValue(new Null());
    }
    Object* obj = (Object*) node;
    const auto& iterator = obj->values.find(property);
    if (iterator == obj->values.end()) {
        return JsonValue(new Null());
    }
    return JsonValue(iterator->second);

}

JsonValue JsonValue::get(int index) {
    if (node->getType().type != ArrayType) {
        return JsonValue(new Null());
    }
    Array* array = (Array*) node;
    if (index < 0 || index >= array->elements.size()) {
        return JsonValue(new Null());
    }
    return JsonValue((array->elements[index]));
}

int JsonValue::asInt() const {
    JsonType type = node->getType().type;
    if (type == NumberLongType) {
        return ((NumberLong*) node)->value;
    }
    if (type == NumberDoubleType) {
        return (int) ((NumberDouble*) node)->value;
    }
    return 0;
}

long JsonValue::asLong() const {
    JsonType type = node->getType().type;
    if (type == NumberLongType) {
        return ((NumberLong*) node)->value;
    }
    if (type == NumberDoubleType) {
        return (long) ((NumberDouble*) node)->value;
    }
    return 0;
}

float JsonValue::asFloat() const {
    JsonType type = node->getType().type;
    if (type == NumberLongType) {
        return (float) ((NumberLong*) node)->value;
    }
    if (type == NumberDoubleType) {
        return (float) ((NumberDouble*) node)->value;
    }
    return 0;
}

double JsonValue::asDouble() const {
    JsonType type = node->getType().type;
    if (type == NumberLongType) {
        return ((NumberLong*) node)->value;
    }
    if (type == NumberDoubleType) {
        return ((NumberDouble*) node)->value;
    }
    return 0;
}

bool JsonValue::asBool() const {
    if (node->getType().type != BoolType) {
        return false;
    }
    return ((Bool*) node)->value;
}

const std::string& JsonValue::asText() const {
    JsonType type = node->getType().type;
    if (type == TextType) {
        return ((Text*) node)->text;
    }
    if (type == NumberLongType) {
        return ((NumberLong*) node)->text;
    }
    if (type == NumberDoubleType) {
        return ((NumberDouble*) node)->text;
    }
    if (type == BoolType) {
        static std::string True = "true";
        static std::string False = "false";
        return ((Bool*) node)->value ? True : False;
    }
    if (type == NullType) {
        static std::string Null = "null";
        return Null;
    }
    static std::string Empty;
    return Empty;
}

int JsonValue::getArrayLength() const {
    expectType(node, Array::Type());
    return ((Array*) node)->elements.size();
}

std::vector<std::string> JsonValue::getObjectKeys() const {
    expectType(node, Object::Type());
    Object* o = (Object*) node;
    return o->keys;
}

JsonValue::JsonValue(Node* node) : node(node) {
    node->incUses();
}

JsonValue::~JsonValue() {
    const JsonTypeInfo& type = getTypeInfo();
    if (node->decUses()) {
        delete node;
    }
}

JsonTypeInfo JsonValue::getTypeInfo() const {
    return node->getType();
}

std::ostream& npjson::operator<<(std::ostream& os, const JsonValue& value) {
    value.node->writeToAsJson(os);
    return os;
}

