//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONTEXT_H
#define HELLOCPP_JSONTEXT_H


#include <string>
#include <ostream>
#include "Node.hpp"

namespace npjson {
    class Text : public Node {
    public:
        const std::string text;

        explicit Text(std::string text);

        void writeToAsJson(std::ostream& os) const override;

        const JsonTypeInfo& getType() const override;

        static const JsonTypeInfo& Type();
    };
}


#endif //HELLOCPP_JSONTEXT_H
