//
// Created by kkolyan on 2021-01-16.
//

#include "Text.hpp"

using namespace npjson;

void Text::writeToAsJson(std::ostream& os) const {
    os << "\"" << text << "\"";
}

const JsonTypeInfo& Text::getType() const {
    return Type();
}

const JsonTypeInfo& Text::Type() {
    static JsonTypeInfo type(TextType, "Text");
    return type;
}

Text::Text(std::string text) :
        text(std::move(text)) {}
