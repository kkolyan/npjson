//
// Created by kkolyan on 2021-01-19.
//

#include "NumberDouble.hpp"

using namespace npjson;

void NumberDouble::writeToAsJson(std::ostream& os) const {
    os << text;
}

const JsonTypeInfo& NumberDouble::getType() const {
    return Type();
}

const JsonTypeInfo& NumberDouble::Type() {
    static JsonTypeInfo type(NumberDoubleType, "NumberDouble");
    return type;
}

NumberDouble::NumberDouble(double value, std::string text)
        : value(value),
          text(std::move(text)) {}
