//
// Created by kkolyan on 2021-01-19.
//

#ifndef HELLOCPP_JSONNUMBERDOUBLE_H
#define HELLOCPP_JSONNUMBERDOUBLE_H

#include <ostream>
#include "Number.hpp"

namespace npjson {
    class NumberDouble : public Number {
    public:
        const double value;
        const std::string text;

        NumberDouble(double value, std::string  text);

        void writeToAsJson(std::ostream& os) const override;

        const JsonTypeInfo& getType() const override;

        static const JsonTypeInfo& Type();
    };
}


#endif //HELLOCPP_JSONNUMBERDOUBLE_H
