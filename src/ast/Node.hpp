//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONNODE_H
#define HELLOCPP_JSONNODE_H

#include <ostream>
#include "JsonTypeInfo.hpp"

namespace npjson {
    class Node {

    public:
        virtual ~Node();

        virtual void writeToAsJson(std::ostream& os) const = 0;

        virtual void incUses();

        virtual bool decUses();

        virtual const JsonTypeInfo& getType() const = 0;

    protected:
        int uses = 0;
    private:
        bool dead = false;
    };
}

#endif //HELLOCPP_JSONNODE_H
