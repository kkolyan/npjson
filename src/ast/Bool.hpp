//
// Created by kkolyan on 2021-01-19.
//

#ifndef HELLOCPP_JSONBOOL_H
#define HELLOCPP_JSONBOOL_H

#include <ostream>
#include "Node.hpp"

namespace npjson {
    class Bool : public Node {
    public:
        const bool value;

        explicit Bool(bool value);

        void writeToAsJson(std::ostream& os) const override;

        const JsonTypeInfo& getType() const override;

        static const JsonTypeInfo& Type();
    };
}


#endif //HELLOCPP_JSONBOOL_H
