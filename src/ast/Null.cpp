//
// Created by kkolyan on 2021-01-19.
//

#include "Null.hpp"

void npjson::Null::writeToAsJson(std::ostream& os) const {
    os << "null";
}

const npjson::JsonTypeInfo& npjson::Null::getType() const {
    return Type();
}

const npjson::JsonTypeInfo& npjson::Null::Type() {
    static JsonTypeInfo type(NullType, "Null");
    return type;
}
