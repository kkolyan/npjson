//
// Created by kkolyan on 2021-01-16.
//

#include "Array.hpp"
#include "../writeWithDelimiter.hpp"

using namespace npjson;

void Array::writeToAsJson(std::ostream& os) const {
    os << "[";
    npjson::writeWithDelimiter(os, ", ", elements, [](std::ostream& os, Node* element) {
        element->writeToAsJson(os);
    });
    os << "]";
}

void Array::incUses() {
    for (Node* element: elements) {
        element->incUses();
    }
    Node::incUses();
}

bool Array::decUses() {

    bool toBeDeleted = Node::decUses();
    if (toBeDeleted) {
        for (Node* element: elements) {
            if (element->decUses()) {
                delete element;
            }
        }
    } else {
        for (Node* element: elements) {
            // do not even check to delete, because child's use counter is never less that parent's
            element->decUses();
        }
    }
    return toBeDeleted;
}

const JsonTypeInfo& Array::getType() const {
    return Type();
}

const JsonTypeInfo& Array::Type() {
    static JsonTypeInfo type(ArrayType, "Array");
    return type;
}
