//
// Created by kkolyan on 2021-01-16.
//

#include "Object.hpp"
#include "../writeWithDelimiter.hpp"

using namespace npjson;


void Object::writeToAsJson(std::ostream& os) const {
    os << "{";
    npjson::writeWithDelimiter(os, ", ", keys, [this](std::ostream& os, const std::string& key) {
        Node* value = this->values.at(key);
        os << "\"";
        os << key;
        os << "\": ";
        value->writeToAsJson(os);
    });
    os << "}";
}

void Object::incUses() {
    for (const std::pair<const std::basic_string<char>, Node*>& entry: values) {
        entry.second->incUses();
    }
    Node::incUses();
}

bool Object::decUses() {
    bool toBeDeleted = Node::decUses();

    if (toBeDeleted) {
        for (const std::pair<const std::basic_string<char>, Node*>& entry: values) {
            if (entry.second->decUses()) {
                delete entry.second;
            }
        }
    } else {
        for (const std::pair<const std::basic_string<char>, Node*>& entry: values) {
            entry.second->decUses();
        }
    }
    return toBeDeleted;
}

const JsonTypeInfo& Object::getType() const {
    return Type();
}

const JsonTypeInfo& Object::Type() {
    static JsonTypeInfo type(ObjectType, "Object");
    return type;
}
