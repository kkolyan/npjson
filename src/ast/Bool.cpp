//
// Created by kkolyan on 2021-01-19.
//

#include "Bool.hpp"

using namespace npjson;

void Bool::writeToAsJson(std::ostream& os) const {
    if (value) {
        os << "true";
    } else {
        os << "false";
    }
}

const JsonTypeInfo& Bool::getType() const {
    return Type();
}

const JsonTypeInfo& Bool::Type() {
    static JsonTypeInfo type(BoolType, "Bool");
    return type;
}

Bool::Bool(bool value) :
        value(value) {}
