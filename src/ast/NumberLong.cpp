//
// Created by kkolyan on 2021-01-19.
//

#include "NumberLong.hpp"

using namespace npjson;

void NumberLong::writeToAsJson(std::ostream& os) const {
    os << text;
}

const JsonTypeInfo& NumberLong::getType() const {
    return Type();
}

const JsonTypeInfo& NumberLong::Type() {
    static JsonTypeInfo type(NumberLongType, "NumberLong");
    return type;
}

NumberLong::NumberLong(const long value, std::string text)
        : value(value),
          text(std::move(text)) {}
