//
// Created by kkolyan on 2021-01-19.
//

#ifndef HELLOCPP_JSONNULL_H
#define HELLOCPP_JSONNULL_H


#include "Node.hpp"

namespace npjson {
    class Null : public Node {

    public:
        void writeToAsJson(std::ostream& os) const override;

        const JsonTypeInfo& getType() const override;

        static const JsonTypeInfo& Type();
    };
}


#endif //HELLOCPP_JSONNULL_H
