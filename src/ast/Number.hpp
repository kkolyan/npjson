//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONNUMBER_H
#define HELLOCPP_JSONNUMBER_H

#include <string>
#include <ostream>
#include "Node.hpp"

namespace npjson {
    class Number : public Node {
    public:
        void writeToAsJson(std::ostream& os) const override = 0;
    };
}


#endif //HELLOCPP_JSONNUMBER_H
