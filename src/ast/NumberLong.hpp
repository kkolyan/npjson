//
// Created by kkolyan on 2021-01-19.
//

#ifndef HELLOCPP_JSONNUMBERLONG_H
#define HELLOCPP_JSONNUMBERLONG_H

#include <ostream>
#include "Number.hpp"

namespace npjson {

    class NumberLong : public Number {
    public:
        const long value;
        const std::string text;

        NumberLong(const long value, std::string  text);

        void writeToAsJson(std::ostream& os) const override;

        const JsonTypeInfo& getType() const override;

        static const JsonTypeInfo& Type();
    };
}

#endif //HELLOCPP_JSONNUMBERLONG_H
