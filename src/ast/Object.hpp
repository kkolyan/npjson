//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONOBJECT_H
#define HELLOCPP_JSONOBJECT_H

#include <unordered_map>
#include <string>
#include <vector>
#include <ostream>
#include "Node.hpp"

namespace npjson {
    class Object : public Node {
    public:
        std::vector<std::string> keys;
        std::unordered_map<std::string, Node*> values;

        void writeToAsJson(std::ostream& os) const override;

        void incUses() override;

        bool decUses() override;

        const JsonTypeInfo& getType() const override;

        static const JsonTypeInfo& Type();
    };
}

#endif //HELLOCPP_JSONOBJECT_H
