//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONARRAY_H
#define HELLOCPP_JSONARRAY_H

#include <vector>
#include <ostream>
#include "Node.hpp"

namespace npjson {
    class Array : public Node {
    public:
        std::vector<Node*> elements;

        void writeToAsJson(std::ostream& os) const override;

        void incUses() override;

        bool decUses() override;

        const JsonTypeInfo& getType() const override;

        static const JsonTypeInfo& Type();
    };
}

#endif //HELLOCPP_JSONARRAY_H
