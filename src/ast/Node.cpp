//
// Created by kkolyan on 2021-01-16.
//

#include "Node.hpp"

void npjson::Node::writeToAsJson(std::ostream& os) const {}

void npjson::Node::incUses() {
    if (dead) {
        throw std::invalid_argument("cannot resurrect dead value");
    }
    uses++;
}

bool npjson::Node::decUses() {
    uses--;
    if (uses <= 0) {
        dead = true;
        return true;
    }
    return false;
}

npjson::Node::~Node() = default;
