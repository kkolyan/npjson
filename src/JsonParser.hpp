//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONPARSER_H
#define HELLOCPP_JSONPARSER_H

#include <iostream>
#include <fstream>
#include "ast/Node.hpp"
#include "ast/Number.hpp"
#include "ast/Text.hpp"
#include "ast/Object.hpp"
#include "ast/Array.hpp"
#include "ast/Bool.hpp"
#include "ast/NumberLong.hpp"
#include "ast/NumberDouble.hpp"
#include "ast/Null.hpp"
#include "JsonValue.hpp"

namespace npjson {
    class JsonParser {
    public:
        explicit JsonParser(const std::string& fileName);

        JsonValue parse();

    private:
        std::ifstream in;
        std::string buffer;
        std::string current;
        bool eof = false;

        void nextToken(int length);

        Node* parseNode();

        Array* parseArray();

        Object* parseObject();

        std::string parseText();

        void expect(const std::string& s) const;

        void rollback();

        Number* parseNumber();

        char nextChar();
    };
}


#endif //HELLOCPP_JSONPARSER_H
