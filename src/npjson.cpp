//
// Created by kkolyan on 2021-01-20.
//


#include "npjson.hpp"
#include "JsonParser.hpp"

npjson::JsonValue npjson::parseFile(const std::string& fileName) {
    return npjson::JsonParser(fileName).parse();
}