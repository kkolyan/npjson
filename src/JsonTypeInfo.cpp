//
// Created by kkolyan on 2021-01-20.
//

#include <string>
#include "JsonTypeInfo.hpp"

using namespace npjson;

JsonTypeInfo::JsonTypeInfo(JsonType type, const std::string& displayName)
        : type(type),
          displayName(displayName) {}

bool JsonTypeInfo::operator==(const JsonTypeInfo& rhs) const {
    return type == rhs.type;
}

bool JsonTypeInfo::operator!=(const JsonTypeInfo& rhs) const {
    return type != rhs.type;
}
