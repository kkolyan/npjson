//
// Created by kkolyan on 2021-01-16.
//

#include "JsonParser.hpp"

using namespace npjson;


JsonParser::JsonParser(const std::string& fileName) : in(fileName) {
    if (!in.is_open()) {
        throw std::invalid_argument("cannot open file: " + fileName);
    }
}

JsonValue JsonParser::parse() {
    return JsonValue(parseNode());
}

Node* JsonParser::parseNode() {
    nextToken(1);
    if (current == "[") {
        return parseArray();
    }
    if (current == "{") {
        return parseObject();
    }
    if (current == "\"") {
        return new Text(parseText());
    }
    rollback();
    nextToken(4);
    if (current == "true") {
        return new Bool(true);
    }
    if (current == "null") {
        return new Null();
    }
    rollback();

    nextToken(5);
    if (current == "false") {
        return new Bool(false);
    }
    rollback();

    return this->parseNumber();
}

Array* JsonParser::parseArray() {
    expect("[");
    Array* array = new Array();
    int n = 0;
    while (true) {
        nextToken(1);
        if (current == "]") {
            break;
        }
        if (n++ > 0) {
            expect(",");
        } else {
            // other than "," and "]" are the part of next element
            rollback();
        }
        Node* node = parseNode();
        array->elements.push_back(node);
    }
    return array;
}

Object* JsonParser::parseObject() {
    expect("{");
    Object* object = new Object();
    int n = 0;
    while (true) {
        nextToken(1);
        if (current == "}") {
            break;
        }
        if (n++ > 0) {
            expect(",");
        } else {
            // other than "," and "}" are the part of next element
            rollback();
        }
        nextToken(1);
        std::string key = parseText();

        nextToken(1);
        expect(":");

        Node* value = parseNode();

        object->keys.push_back(key);
        object->values[key] = value;
    }
    return object;
}

std::string JsonParser::parseText() {
    expect("\"");
    std::string text;
    while (true) {
        nextToken(1);
        if (current == "\"") {
            break;
        }
        text.append(current);
    }
    return text;
}

void JsonParser::nextToken(int length) {
    current = "";
    while (true) {
        if (current.length() >= length) {
            break;
        }
        char c = nextChar();
        if (c < 0) {
            break;
        }

        if (std::isspace(c)) {
            continue;
        }
        current.append(1, c);
    }
}

char JsonParser::nextChar() {
    if (buffer.empty()) {
        if (eof) {
            return -1;
        }
        char buf[1024];
        if (!in.read(buf, 1024)) {
            eof = true;
        }
        std::streamsize n = in.gcount();
        buffer.append(buf, n);
    }
    char c = buffer.front();
    buffer.erase(0, 1);
    return c;
}

void JsonParser::expect(const std::string& s) const {
    if (current != s) {
        throw std::invalid_argument("unexpected token: " + current);
    }
}

void JsonParser::rollback() {
    buffer = current + buffer;
    current = "";
}

Number* JsonParser::parseNumber() {
    // skip whitespace and get try sign
    nextToken(1);
    int sign = 1;
    std::string text;
    if (current == "-") {
        text.append(current);
        sign = -1;
    } else {
        rollback();
    }
    long number = 0;
    bool fractionExpected = false;

    while (true) {
        char c = nextChar();
        if (c < 0) {
            break;
        }
        if (c == '.') {
            text.append(1, c);
            fractionExpected = true;
            break;
        }
        if (!std::isdigit(c)) {
            buffer.insert(0, &c, 1);
            break;
        }
        text.append(1, c);
        number *= 10;
        number += (c - '0');
    }
    if (!fractionExpected) {
        return new NumberLong(sign * number, text);
    }

    int fractionDigits = 0;
    long fractionValue = 0;

    while (true) {
        char c = nextChar();
        if (c < 0) {
            break;
        }
        if (!std::isdigit(c)) {
            buffer.insert(0, &c, 1);
            break;
        }
        text.append(1, c);
        fractionValue *= 10;
        fractionValue += (c - '0');
        fractionDigits++;
    }

    double value = sign * number + 1.0 * fractionValue / fractionDigits;
    return new NumberDouble(value, text);
}
