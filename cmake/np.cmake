
function(np_auto_target_sources TARGET_NAME)
    file(GLOB_RECURSE sources *.cpp)
    target_sources(${TARGET_NAME} PRIVATE ${sources})
endfunction()