#include <iostream>

#include "npjson.hpp"

void parseAndPrintFile(const std::string& file);

int main() {
    std::string file = "demo.json";

    std::cout << "parsing \"" << file << "\"..." << std::endl;

    parseAndPrintFile(file);

    std::cout << "...parsing complete!" << std::endl;
    return 0;
}

void parseAndPrintFile(const std::string& file) {
    npjson::JsonValue json = npjson::parseFile(file);
    std::cout << json << std::endl;
}
